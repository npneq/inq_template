/* -*- indent-tabs-mode: t -*- */

// Copyright (C) 2019-2023 Lawrence Livermore National Security, LLC., Xavier Andrade, Alfredo A. Correa
//
// Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include <inq/inq.hpp>

using namespace inq;
using namespace inq::magnitude;

int main(int argc, char ** argv){
	
	auto env = input::environment{};
	
	auto distance = 1.09_angstrom;

	auto ions = systems::ions(systems::cell::orthorhombic(10.0_b, 10.0_b, 12.0_b));
	ions.insert("N", {0.0_b, 0.0_b, -distance/2});
	ions.insert("N", {0.0_b, 0.0_b,  distance/2});

	auto electrons = systems::electrons(env.par(), ions, options::electrons{}.cutoff(30.0_Ha));

	ground_state::initial_guess(ions, electrons);
	auto result = ground_state::calculate(ions, electrons, options::theory{}.pbe());

	if(env.par().comm().root()) std::cout << "The total energy is " << result.energy.total() << " Hartree " << std::endl;
	
}
